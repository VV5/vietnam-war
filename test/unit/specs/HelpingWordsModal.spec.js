import { shallowMount } from '@vue/test-utils'
import HelpingWordsModal from '@/components/HelpingWordsModal'

describe('HelpingWordsModal', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(HelpingWordsModal)
  })

  test('should close the modal when modal background is clicked', () => {
    wrapper.find('.modal-background').trigger('click')

    wrapper.vm.$emit('closeModal')

    expect(wrapper.emitted().closeModal).toBeTruthy()
  })

  test('should close the modal when close button is clicked', () => {
    wrapper.find('button.delete').trigger('click')

    wrapper.vm.$emit('closeModal')

    expect(wrapper.emitted().closeModal).toBeTruthy()
  })
})
