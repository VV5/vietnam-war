import { shallowMount, createLocalVue } from '@vue/test-utils'
import sinon from 'sinon'
import Vuex from 'vuex'
import Conclusion from '@/components/Conclusion'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('Conclusion', () => {
  let wrapper, store

  beforeEach(() => {
    store = new Vuex.Store({})

    wrapper = shallowMount(Conclusion, {
      store, localVue
    })
  })

  test('', () => {
    const windowPrintStub = sinon.stub(window, 'print')

    wrapper.find('.is-print').trigger('click')

    expect(windowPrintStub.called).toBe(true)
  })
})
