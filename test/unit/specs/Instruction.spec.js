import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'
import Instruction from '@/components/Instruction'

const localVue = createLocalVue()
localVue.use(VueRouter)

describe('Instruction.vue', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(Instruction)
  })

  test('should render correct contents', () => {
    expect(wrapper.find('h1').text()).toBe('Instruction')
  })

  test('should render correct button text', () => {
    expect(wrapper.find('button').text()).toBe('Proceed')
  })

  test('should navigate to correct path', () => {
    const routes = [
      {
        path: '/activity'
      }
    ]

    const router = new VueRouter({
      routes
    })

    const wrapper = shallowMount(Instruction, {
      router, localVue
    })

    wrapper.find('button').trigger('click')

    expect(wrapper.vm.$route.path).toBe('/activity')
  })
})
