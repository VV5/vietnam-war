import { shallowMount, createLocalVue } from '@vue/test-utils'
import sinon from 'sinon'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import SummaryView from '@/components/SummaryView'
import HelpingWordsModal from '@/components/HelpingWordsModal'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('SummaryView', () => {
  let wrapper
  let store, state, mutations, getters
  let router

  beforeEach(() => {
    state = {}

    mutations = {
      setResponse: jest.fn()
    }

    getters = {
      presidentPhoto: jest.fn()
    }

    store = new Vuex.Store({
      state, mutations, getters
    })

    router = new VueRouter()

    wrapper = shallowMount(SummaryView, {
      attachToDocument: true,
      store,
      router,
      localVue
    })
  })

  test('should close modal when ESC button is pressed', () => {
    wrapper.setData({ isActive: true })

    wrapper.trigger('keyup', { key: 'Escape' })

    expect(wrapper.vm.isActive).toBe(false)
  })

  test('should not close modal when buttons other than ESC is pressed', () => {
    wrapper.setData({ isActive: true })

    wrapper.trigger('keyup')

    expect(wrapper.vm.isActive).toBe(true)
  })

  test('should open helping words modal when helping words button is clicked', () => {
    wrapper.find('.open-modal').trigger('click')

    expect(wrapper.vm.isActive).toBe(true)
  })

  test('should close helping words modal', () => {
    wrapper.find(HelpingWordsModal).vm.$emit('closeModal')

    expect(wrapper.vm.isActive).toBe(false)
  })

  test('should navigate to conclusion page', () => {
    const $route = {
      path: '/conclusion'
    }

    const router = new VueRouter()

    wrapper = shallowMount(SummaryView, {
      store,
      router,
      localVue,
      computed: {
        isOverallCompleted: () => true
      }
    })

    wrapper.find('.is-submit').trigger('click')

    expect(mutations.setResponse).toHaveBeenCalled()

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
