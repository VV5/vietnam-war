import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'
import Introduction from '@/components/Introduction'

const localVue = createLocalVue()
localVue.use(VueRouter)

describe('Introduction.vue', () => {
  test('should render correct contents', () => {
    const wrapper = shallowMount(Introduction)

    expect(wrapper.find('h1').text()).toBe('The Vietnam War')
  })

  test('should navigate correctly', () => {
    const routes = [
      {
        path: '/instruction'
      }
    ]

    const router = new VueRouter({
      routes
    })

    const wrapper = shallowMount(Introduction, {router, localVue})

    wrapper.find('button').trigger('click')

    expect(wrapper.vm.$route.path).toBe('/instruction')
  })
})
