import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import App from '@/App'
import config from '../../../package'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('App', () => {
  let store, mutations

  beforeEach(() => {
    mutations = {
      setVersion: jest.fn()
    }

    store = new Vuex.Store({
      mutations
    })

    shallowMount(App, {
      store, localVue
    })
  })

  it('should render the correct version', () => {
    expect(mutations.setVersion.mock.calls).toHaveLength(1)
    expect(mutations.setVersion.mock.calls[0][1]).toEqual(config.version)
  })
})
