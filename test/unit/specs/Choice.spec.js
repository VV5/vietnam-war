import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import Choice from '@/components/Choice'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('Choice', () => {
  let wrapper, store, state, mutations

  beforeEach(() => {
    state = {}

    mutations = {
      selectOption: jest.fn()
    }

    store = new Vuex.Store({
      state, mutations
    })

    wrapper = shallowMount(Choice, {
      store,
      localVue,
      propsData: {
        button: {
          id: 1,
          name: 'Withdraw'
        }
      }
    })
  })

  test('', () => {
    wrapper.find('.is-option').trigger('click')

    expect(mutations.selectOption).toHaveBeenCalled()
  })
})
