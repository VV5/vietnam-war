import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import PresidentSummary from '@/components/PresidentSummary'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('PresidentSummary', () => {
  let wrapper
  let store, state

  beforeEach(() => {
    state = {}

    store = new Vuex.Store({
      state
    })

    wrapper = shallowMount(PresidentSummary, {
      store,
      localVue,
      propsData: {
        president: {}
      }
    })
  })

  test('should trigger text change when each character is typed', () => {
    const testPhrase = 'Hello World!'

    const president = wrapper.findAll('.has-response').at(0)
    president.element.value = testPhrase
    president.trigger('keyup')

    wrapper.vm.$emit('textChange')

    expect(wrapper.emitted().textChange).toBeTruthy()
  })
})
