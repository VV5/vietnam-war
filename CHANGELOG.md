# Changelog

## v2.0.3
- Updated packages with breaking changes

## v2.0.2
- Fixed path for fonts not loading correctly
- Optimised webpack configuration for production

## v2.0.1
- Fixed background images not loading with relative paths
- Removed e2e stuff from packages and tests
- Fixed relative paths to open index.html locally

## v2.0.0
- Upgraded Webpack to v4

## v1.0.1
- Added corresponding symbols for correct and wrong answers

## v1.0.0
- Added colour to button

## v1.0.0-beta.4
- Added save/print as PDF
- Added displaying of responses on the last page
- Added function to display responses upon completion of activity
- Fixed an issue with option buttons not disabled when the correct option is selected resulting in a "dead-end"

## v1.0.0-beta.3
- Fixed buttons colours scheme
- Added a function to disable button when option selected is wrong

## v1.0.0-beta.2
- Added new assets and removed the old ones
- Fixed broken path issues with assets
- Fixed an issue with status bar not toggling correctly at certain pages
- Fixed an issue with buttons not toggling it state correctly
- Fixed an issue with presidents' photo not being rendered correctly
