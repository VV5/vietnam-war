import Vue from 'vue'
import Router from 'vue-router'
import Introduction from '@/components/Introduction'
import Instruction from '@/components/Instruction'
import Activity from '@/components/Activity'
import SummaryView from '@/components/SummaryView'
import Conclusion from '@/components/Conclusion'

Vue.use(Router)

export default new Router({
  base: window.location.pathname,
  routes: [
    {
      path: '/',
      name: 'Introduction',
      component: Introduction,
      meta: {
        title: 'Welcome'
      }
    },
    {
      path: '/instruction',
      name: 'Instruction',
      component: Instruction,
      meta: {
        title: 'Instruction'
      }
    },
    {
      path: '/activity',
      name: 'Activity',
      component: Activity,
      meta: {
        title: 'Activity'
      }
    },
    {
      path: '/summary',
      name: 'Summary',
      component: SummaryView,
      meta: {
        title: 'Summary'
      }
    },
    {
      path: '/conclusion',
      name: 'Conclusion',
      component: Conclusion,
      meta: {
        title: 'Thank you'
      }
    }
  ]
})
