import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    version: null,
    showStatusBar: false,
    isAnswered: false,
    selectedSlide: 0,
    selectedOption: {
      index: 0,
      answer: null
    },
    slides: [
      {
        id: 1,
        date: 'Nov 1963',
        president: 1,
        question: 'You have just taken over as the President of the United States after the assassination of President John F. Kennedy.',
        answers: {
          withdraw: {
            text: '<p>The manner in which Johnson came to the presidency meant that he would have been pre-occupied with the aftermath of Kennedy’s assassination and the transition of government. Vietnam was probably much less of a priority for Johnson at that time. However, like Kennedy, Johnson also believed in the domino theory, which claimed that allowing Vietnam to fall to the Communists would mean allowing the rest of Southeast Asia to turn Communist. <b><u>As such, he would be unlikely to reverse Kennedy’s policy on Vietnam. Neither would Johnson have escalated the war as he would have wanted time to assess the issue first.</u></b></p>',
            correct: false
          },
          stay: {
            text: '<p>The manner in which Johnson came to the presidency meant that he would have been pre-occupied with the aftermath of Kennedy’s assassination and the transition of government. Vietnam was probably much less of a priority for Johnson at that time. However, like Kennedy, Johnson also believed in the domino theory, which claimed that allowing Vietnam to fall to the Communists would mean allowing the rest of Southeast Asia to turn Communist. <b><u>As such, he would be unlikely to reverse Kennedy’s policy on Vietnam. Neither would Johnson have escalated the war as he would have wanted time to assess the issue first.</u></b></p>',
            correct: true
          },
          escalate: {
            text: '<p>The manner in which Johnson came to the presidency meant that he would have been pre-occupied with the aftermath of Kennedy’s assassination and the transition of government. Vietnam was probably much less of a priority for Johnson at that time. However, like Kennedy, Johnson also believed in the domino theory, which claimed that allowing Vietnam to fall to the Communists would mean allowing the rest of Southeast Asia to turn Communist. <b><u>As such, he would be unlikely to reverse Kennedy’s policy on Vietnam. Neither would Johnson have escalated the war as he would have wanted time to assess the issue first.</u></b></p>',
            correct: false
          }
        }
      },
      {
        id: 2,
        date: 'Sep 1964',
        president: 1,
        question: 'You are running to be elected President in your own right. Your campaign is based on a platform of domestic reform, called “The Great Society”, encompassing environmental, anti-poverty, healthcare and crime control initiatives. Your opponent is Barry Goldwater, a strong anti-Communist who urged the use of nuclear weapons in Vietnam.',
        answers: {
          withdraw: {
            text: '<p>Johnson was keen to advance his domestic agenda and downplay Vietnam. Given that his opponent, Barry Goldwater, was a well-known hawk on the Cold War, Johnson could paint him as a dangerous fanatic who might lead the world into nuclear war. <b><u>As such, Johnson would be unlikely to escalate the war in Vietnam as this would appear hypocritical to US voters. However, he would also be seen as being soft on Communism if he were to reduce US involvement in Vietnam. Thus, he would most probably stay the course, at least until the election was over.</u></b></p>',
            correct: false
          },
          stay:
            {
              text: '<p>Johnson was keen to advance his domestic agenda and downplay Vietnam. Given that his opponent, Barry Goldwater, was a well-known hawk on the Cold War, Johnson could paint him as a dangerous fanatic who might lead the world into nuclear war. <b><u>As such, Johnson would be unlikely to escalate the war in Vietnam as this would appear hypocritical to US voters. However, he would also be seen as being soft on Communism if he were to reduce US involvement in Vietnam. Thus, he would most probably stay the course, at least until the election was over.</u></b></p>',
              correct: true
            },
          escalate: {
            text: '<p>Johnson was keen to advance his domestic agenda and downplay Vietnam. Given that his opponent, Barry Goldwater, was a well-known hawk on the Cold War, Johnson could paint him as a dangerous fanatic who might lead the world into nuclear war. <b><u>As such, Johnson would be unlikely to escalate the war in Vietnam as this would appear hypocritical to US voters. However, he would also be seen as being soft on Communism if he were to reduce US involvement in Vietnam. Thus, he would most probably stay the course, at least until the election was over.</u></b></p>',
            correct: false
          }
        }
      },
      {
        id: 3,
        date: 'Sep 1965',
        president: 1,
        question: 'Civil war broke out in the Dominican Republic. You had to send in US Marines to protect US civilians and the embassy in the country. US intervention helped to secure a quick and favourable end to the Dominican civil war.',
        answers: {
          withdraw: {
            text: 'To Johnson, the Dominican civil war showed that it was possible to pacify a foreign country and put down a rebellion through external intervention. <b><u>This would have increased Johnson’s resolve to continue or even escalate US involvement in Vietnam to achieve a similar outcome.</u></b>',
            correct: false
          },
          stay: {
            text: 'To Johnson, the Dominican civil war showed that it was possible to pacify a foreign country and put down a rebellion through external intervention. <b><u>This would have increased Johnson’s resolve to continue or even escalate US involvement in Vietnam to achieve a similar outcome.</u></b>',
            correct: false
          },
          escalate: {
            text: 'To Johnson, the Dominican civil war showed that it was possible to pacify a foreign country and put down a rebellion through external intervention. <b><u>This would have increased Johnson’s resolve to continue or even escalate US involvement in Vietnam to achieve a similar outcome.</u></b>',
            correct: true
          }
        }
      },
      {
        id: 4,
        date: 'Jun 1967',
        president: 1,
        question: 'Opinion polls shows that the majority of the US public disapproves of your handling of the Vietnam War. However, intelligence reports from the Central Intelligence Agency suggest that the Viet Minh have been suffering heavily from the bombing.',
        answers: {
          withdraw: {
            text: 'While the US public had started to turn against the war, <b><u>Johnson would have been wary of stopping or withdrawing US involvement in Vietnam without having achieved any credible outcomes.</u></b> However, Johnson would have been aware of the need to pursue a swift and positive conclusion to the war, which the CIA reports had given hope to. Thus <b><u>it was more likely that Johnson would have escalated US involvement in Vietnam to try to gain a quick and decisive victory.</u></b>',
            correct: false
          },
          stay: {
            text: 'While the US public had started to turn against the war, <b><u>Johnson would have been wary of stopping or withdrawing US involvement in Vietnam without having achieved any credible outcomes.</u></b> However, Johnson would have been aware of the need to pursue a swift and positive conclusion to the war, which the CIA reports had given hope to. Thus <b><u>it was more likely that Johnson would have escalated US involvement in Vietnam to try to gain a quick and decisive victory.</u></b>',
            correct: false
          },
          escalate: {
            text: 'While the US public had started to turn against the war, <b><u>Johnson would have been wary of stopping or withdrawing US involvement in Vietnam without having achieved any credible outcomes.</u></b> However, Johnson would have been aware of the need to pursue a swift and positive conclusion to the war, which the CIA reports had given hope to. Thus <b><u>it was more likely that Johnson would have escalated US involvement in Vietnam to try to gain a quick and decisive victory.</u></b>',
            correct: true
          }
        }
      },
      {
        id: 5,
        date: 'Sep 1968',
        president: 1,
        question: 'The Viet Minh launches the Tet Offensive, attacking major cities in South Vietnam, including the US embassy in Saigon. The attack came as a major surprise, although the Viet Minh were beaten back. However, the attack has turned the US public decisively against the war.',
        answers: {
          withdraw: {
            text: 'The Tet Offensive suggested that victory in Vietnam was neither guaranteed nor imminent. <b><u>Johnson realised that the war could not be won and a way had to be found to achieve peace without appearing to give in to the Communists.</u></b>',
            correct: true
          },
          stay: {
            text: 'The Tet Offensive suggested that victory in Vietnam was neither guaranteed nor imminent. <b><u>Johnson realised that the war could not be won and a way had to be found to achieve peace without appearing to give in to the Communists.</u></b>',
            correct: false
          },
          escalate: {
            text: 'The Tet Offensive suggested that victory in Vietnam was neither guaranteed nor imminent. <b><u>Johnson realised that the war could not be won and a way had to be found to achieve peace without appearing to give in to the Communists.</u></b>',
            correct: false
          }
        }
      },
      {
        id: 6,
        date: 'Jan 1969',
        president: 2,
        question: 'You have just been elected as US President, taking over from Lyndon B. Johnson. During the election campaign, you promised to end the war in Vietnam and bring about “peace with honour”.',
        answers: {
          withdraw: {
            text: '<b><u>Having made the election promise to withdraw from Vietnam, Nixon had to move quickly to fulfil it. Otherwise he risked the anger of the US public and eroding his credibility as President.</u></b> Thus Nixon initiated the process of “Vietnamisation”, which meant handing the responsibility for fighting North Vietnam back to the South Vietnamese government.',
            correct: true
          },
          stay: {
            text: '<b><u>Having made the election promise to withdraw from Vietnam, Nixon had to move quickly to fulfil it. Otherwise he risked the anger of the US public and eroding his credibility as President.</u></b> Thus Nixon initiated the process of “Vietnamisation”, which meant handing the responsibility for fighting North Vietnam back to the South Vietnamese government.',
            correct: false
          },
          escalate: {
            text: '<b><u>Having made the election promise to withdraw from Vietnam, Nixon had to move quickly to fulfil it. Otherwise he risked the anger of the US public and eroding his credibility as President.</u></b> Thus Nixon initiated the process of “Vietnamisation”, which meant handing the responsibility for fighting North Vietnam back to the South Vietnamese government.',
            correct: false
          }
        }
      },
      {
        id: 7,
        date: 'Jan 1969',
        president: 2,
        question: 'You visit the People\'s Republic of China, the first US President to do so since the Communist Party defended the Nationalists in 1949. The visit is a success, ushering a new era in Sino-American relations.',
        answers: {
          withdraw: {
            text: 'Improved relations with China helped to reduce the fear of a Community takeover of Southeast Asia should the US withdraw from Vietnam. Since the Communist threat was now receding, there was little argument for the US to remain in Vietnam. <b><u>This would have increased the likelihood of Nixon disengaging the US from Vietnam.</u></b>',
            correct: true
          },
          stay: {
            text: 'Improved relations with China helped to reduce the fear of a Community takeover of Southeast Asia should the US withdraw from Vietnam. Since the Communist threat was now receding, there was little argument for the US to remain in Vietnam. <b><u>This would have increased the likelihood of Nixon disengaging the US from Vietnam.</u></b>',
            correct: false
          },
          escalate: {
            text: 'Improved relations with China helped to reduce the fear of a Community takeover of Southeast Asia should the US withdraw from Vietnam. Since the Communist threat was now receding, there was little argument for the US to remain in Vietnam. <b><u>This would have increased the likelihood of Nixon disengaging the US from Vietnam.</u></b>',
            correct: false
          }
        }
      }
    ],
    responses: [],
    summaryQuestion: 'What considerations and circumstances shaped US involvement in Vietnam under Presidents Johnson and Nixon? How did they affect the Presidents’ decisions on Vietnam?'
  },
  mutations: {
    setVersion (state, version) {
      state.version = version
    },
    clearResponseInput (state) {
      state.responses = []
    },
    setStatusBar (state, status) {
      state.showStatusBar = status
    },
    selectOption (state, payload) {
      switch (payload.type) {
        case 1:
          state.selectedOption = {
            answer: state.slides[payload.i].answers.withdraw
          }
          break
        case 2:
          state.selectedOption = {
            answer: state.slides[payload.i].answers.stay
          }
          break
        case 3:
          state.selectedOption = {
            answer: state.slides[payload.i].answers.escalate
          }
          break
        default:
          break
      }

      state.isAnswered = true

      return state.selectedOption
    },
    nextSlide (state) {
      state.selectedSlide++
      state.selectedOption.answer = null
      state.isAnswered = false
    },
    setResponse (state, response) {
      state.responses = response
    }
  },
  getters: {
    presidentPhoto (state) {
      const arrayPresidentPhotos = ['president-lyndon-johnson.png', 'president-richard-nixon.png']
      const presidentId = state.slides[state.selectedSlide].president

      return arrayPresidentPhotos[presidentId - 1]
    },
    presidentName (state) {
      const arrayPresidentNames = ['Lyndon B. Johnson', 'Richard Nixon']
      const presidentId = state.slides[state.selectedSlide].president

      return arrayPresidentNames[presidentId - 1]
    }
  }
})
